from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, PageBreak, Frame, Spacer
from reportlab.platypus import Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_JUSTIFY, TA_RIGHT
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm, inch, mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus.doctemplate import Indenter
from reportlab.lib.utils import ImageReader
from reportlab.platypus.flowables import Image
from io import StringIO, BytesIO
from datetime import datetime
import urllib.request as urllib2
from PIL import Image as I1
import requests
import gc
import os
import sys

pdfmetrics.registerFont(TTFont('lato_black', "../Flask/lato/Lato-Light.ttf"))
pdfmetrics.registerFont(TTFont('lato_bold', "../Flask/lato/Lato-Bold.ttf"))

data = {
    "code": "200",
    "message": "Success",
    "data": {
        "id": 40,
        "projectId": "-MIUapbi67wpiNCU6rEi",
        "createdAt": "2020-10-14T00:00:00Z",
        "priority": "Low",
        "costImpact": True,
        "scheduleImpact": True,
        "rfiTitle": "Significant trip husband budget.",
        "resolvedByDate": "2020-11-03",
        "category": "query",
        "subCategory": "detail clasifications",
        "rfiState": "open",
        "totalQuestions": 5,
        "totalResponded": 5,
        "rfiSequence": 40,
        "conversation": True,
        "questions": [
            {
                "question": {
                    "id": 119,
                    "questionState": "draft",
                    "questionMsg": "<p>Myself few attorney center. Break member risk pattern. Catch marriage test put person sister technology. Less ago peace girl. Cover program decision find talk ask picture. Maintain old oil upon control relationship middle. Feel check box suddenly. Determine heart though shoulder sign despite but. Final their rather. Hundred section activity property require customer value. Week theory something American likely fine mission. Painting senior brother blood save per fund. Dinner energy else partner agent. Offer interesting organization first keep area industry manager. Magazine like war I task letter purpose protect. According plan scene last beautiful. Old project thing. Wear surface wait watch leg cause speech stop. Member person pay heart in. Decide charge quality about mention former doctor. Drop audience remain draw partner design. Tax along thank hotel break opportunity although. Big staff management movement letter we field.</p>", "createdAt": "2020-10-14T00:00:00Z",
                    "rfiId": 40,
                    "attachments": [
                        {
                            "id": "849176d6-589d-4957-be64-349d6c95e7db",
                            "createdAt": "2020-11-02T12:38:43.006817Z",
                            "projectPlans": True,
                            "sheetCode": "E-2.3",
                            "sheetVersion": "v1"
                        }
                    ]},
                "seqNumber": 1,
                "response": {
                    "id": 63,
                    "creatorId": "lithiumm6754",
                    "responseState": "draft",
                    "createdAt": "2020-11-02T15:02:27.453647Z",
                    "responseMsg": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
                    "rfiId": 40,
                    "questionId": 119,
                    "attachments": [
                            {
                                "id": "069cdda2-9706-4227-9635-286758e3fa07",
                                "createdAt": "2020-11-02T15:02:31.021326Z",
                                "projectPlans": True,
                                "sheetCode": "CF2",
                                "sheetVersion": "v1"
                            },
                        {
                                "id": "48f1fae8-c2ec-4e00-aae4-ce6f677f1782",
                                "createdAt": "2020-11-02T15:02:31.283928Z",
                                "projectPlans": True,
                                "sheetCode": "ATH6",
                                "sheetVersion": "v1"
                                }
                    ]}},
            {
                "question": {
                    "id": 122,
                    "questionState": "draft",
                    "questionMsg": "<p>Most work sea national. Art include important right interesting. During money time feeling. Total president professional central. Dinner none write. Consider also easy. Nice mouth own color year since action. Back maintain yes simple outside. Participant need show there expect truth. Chance cup in. Rate after somebody pattern cut must. To rich level beyond education claim. Billion thousand single civil. Level special south wonder. Attorney fall six visit including. Around town store but. Form smile skill father direction continue exactly. Manage small right make success fight total. Station tonight become occur push. Lawyer firm according reflect. Structure television its contain. Beautiful street majority provide garden term. Seven name use doctor. Car upon success lawyer analysis. Condition stop fight consider leader member. Drop owner want firm. Special role discover ball save. Toward already performance. Here loss risk name subject late moment. Charge seven happy follow.</p>", "createdAt": "2020-10-14T00:00:00Z",
                    "rfiId": 40,
                    "attachments": [
                        {
                            "id": "8e1fbcfb-0c3e-446b-bf79-96834ecf1eab",
                            "createdAt": "2020-11-02T12:36:12.365214Z",
                            "projectPlans": False,
                            "sheetCode": None,
                            "sheetVersion": None
                        },
                        {
                            "id": "b13554b9-6550-49fe-8f15-49c6b04b3ed5",
                            "createdAt": "2020-11-02T12:36:12.681954Z",
                            "projectPlans": False,
                            "sheetCode": None,
                            "sheetVersion": None
                        },
                        {
                            "id": "6dbcd98e-f056-4c8c-bb22-9fc55fc45211",
                            "createdAt": "2020-11-02T12:36:13.107551Z",
                            "projectPlans": True,
                            "sheetCode": "E-2.3",
                            "sheetVersion": "v1"
                        },
                        {
                            "id": "f84bfc8a-4e33-44dc-991b-cdf97d6e963d",
                            "createdAt": "2020-11-02T12:36:14.013152Z",
                            "projectPlans": True,
                            "sheetCode": "ATH6",
                            "sheetVersion": "v1"
                        }
                    ]},
                "seqNumber": 2,
                "response": {
                    "id": 64,
                    "creatorId": "lithiumm6754",
                    "responseState": "draft",
                    "createdAt": "2020-11-02T15:03:15.197794Z",
                    "responseMsg": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
                    "rfiId": 40,
                    "questionId": 122,
                    "attachments": [
                            {
                                "id": "33cfd339-8e47-4543-9d01-058b2ece08ff",
                                "createdAt": "2020-11-02T15:03:15.780745Z",
                                "projectPlans": False,
                                "sheetCode": None,
                                "sheetVersion": None
                            },
                        {
                                "id": "68b7df6e-0e99-456d-9b02-bdf985821050",
                                "createdAt": "2020-11-02T15:03:16.650500Z",
                                "projectPlans": True,
                                "sheetCode": "ANG11",
                                "sheetVersion": "v1"
                                }
                    ]
                }
            },
            {
                "question": {
                    "id": 121,
                    "questionState": "draft",
                    "questionMsg": "Gas according issue cell experience night. Yourself him decade matter building great beat.<br>Side believe owner. Parent half check door national several raise. Fight nice radio drug blue store.<br>Two western analysis western loss. Right money skin mouth.<br>Direction meet between its hotel gun. While protect down.<br>Fly myself career remain from else skin. Build without bed road pressure.<br>Amount firm high value whether magazine natural central. Method PM campaign kid four. President I produce recently best heart.<br>Article civil owner begin. Talk cultural require yourself series. Notice coach avoid discussion situation table because. Special price painting best feeling.<br>Others stop born offer traditional strategy show property. Consumer nature individual the production catch design almost.<br>Support social whom really toward story. Join there north national. Individual stop fill friend student cultural.", "createdAt": "2020-10-14T00:00:00Z",
                    "rfiId": 40,
                    "attachments": []
                },
                "seqNumber":3,
                "response":
                {"id": 65,
                 "creatorId": "lithiumm6754",
                 "responseState": "draft",
                 "createdAt": "2020-11-02T15:03:25.186546Z",
                 "responseMsg": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
                 "rfiId": 40,
                 "questionId": 121,
                 "attachments": []
                 }},
            {
                "question": {
                    "id": 120,
                    "questionState": "draft",
                    "questionMsg": "<p>Thousand class coach decision accept win miss pass. Born blue affect that hair recently. Tv dog hard over crime message. Can inside kitchen report security anything war. Film training pattern. Owner tell result game. Bank he Republican life coach store. Line well cover else. Like charge where task big economy new. Generation student group strategy. Leave start happy pass cost. Tell friend record. Wind film finish collection. Responsibility society I about. Board once above trouble. Bar card son. Special art lawyer camera enter who. Agree life myself experience. Similar factor write huge level. Figure audience I suggest low whatever safe career. Sell for economy yourself. Government win also clear. East financial little never inside state. Executive receive management various yard. Physical toward six. Attorney television power paper need professor. Debate improve happen. Knowledge yes they join. Himself perform economic learn else.</p>",
                    "createdAt": "2020-10-14T00:00:00Z",
                    "rfiId": 40,
                    "attachments": [
                        {
                            "id": "772056e3-c026-4a3e-ba5d-8a7e2e4852b7",
                            "createdAt": "2020-11-02T12:38:14.271922Z",
                            "projectPlans": True,
                            "sheetCode": "E-2.3",
                            "sheetVersion": "v1"
                        }
                    ]},
                "seqNumber": 4,
                "response": {
                    "id": 66,
                    "creatorId": "lithiumm6754",
                    "responseState": "draft",
                    "createdAt": "2020-11-02T15:03:32.239984Z",
                    "responseMsg": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
                    "rfiId": 40,
                    "questionId": 120,
                    "attachments": []
                }
            },
            {
                "question": {
                    "id": 156,
                    "questionState": "draft",
                    "questionMsg": "<p>Myself few attorney center. Break member risk pattern.Catch marriage test put person sister technology. Less ago peace girl.Cover program decision find talk ask picture. Maintain old oil upon control relationship middle. Feel check box suddenly.Determine heart though shoulder sign despite but. Final their rather.Hundred section activity property require customer value. Week theory something American likely fine mission.Painting senior brother blood save per fund. Dinner energy else partner agent.Offer interesting organization first keep area industry manager. Magazine like war I task letter purpose protect. According plan scene last beautiful.Old project thing.Wear surface wait watch leg cause speech stop. Member person pay heart in. Decide charge quality about mention former doctor.Drop audience remain draw partner design. Tax along tasdasdasdasdhank hotel break opportunity although. Big staff management movement letter we field.</p>",
                    "createdAt": "2020-10-19T16:13:29.045903Z",
                    "rfiId": 40,
                    "attachments": [
                        {
                            "id": "d2ae54f1-4431-4ea9-b838-9c99ba52e091",
                            "createdAt": "2020-10-19T16:13:29.505183Z",
                            "projectPlans": False,
                            "sheetCode": None,
                            "sheetVersion": None
                        },
                        {
                            "id": "24adffa5-de7a-4d4d-a981-3dba699cf448",
                            "createdAt": "2020-10-19T16:13:29.530863Z",
                            "projectPlans": False,
                            "sheetCode": None,
                            "sheetVersion": None
                        },
                        {
                            "id": "05706b68-dd0b-4844-8e1f-0e22aa0e6985",
                            "createdAt": "2020-11-02T10:40:24.632581Z",
                            "projectPlans": True,
                            "sheetCode": "ATH6",
                            "sheetVersion": "v1"
                        }
                    ]
                },
                "seqNumber": 5,
                "response": {
                    "id": 67,
                    "creatorId": "lithiumm6754",
                    "responseState": "draft",
                    "createdAt": "2020-11-02T15:04:18.748014Z",
                    "responseMsg": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
                    "rfiId": 40,
                    "questionId": 156,
                    "attachments": [
                        {
                            "id": "410cd21b-2bee-491e-ad9f-cc5bf71f18df",
                            "createdAt": "2020-11-02T15:04:19.843269Z",
                            "projectPlans": True,
                            "sheetCode": "E-2.3",
                            "sheetVersion": "v1"
                        }
                    ]
                }
            }
        ],
        "rfiPersona": [
            {
                "userId": "lithiumm6754",
                "companyId": "level1c8603",
                "userRole": "foreman",
                "userRfiRole": "rfiCreator"
            },
            {
                "userId": "carbonh3982",
                "companyId": "level1c8603",
                "userRole": "projectManager",
                "userRfiRole": "rfiCreator"
            },
            {
                "userId": "eriks9781",
                "companyId": "zenb9315",
                "userRole": "projectManager",
                "userRfiRole": "rfiManager"
            },
            {
                "userId": "carbonh3982",
                "companyId": "level1c8603",
                "userRole": "projectManager",
                "userRfiRole": "rfiReceiver"
            },
            {
                "userId": "lithiumm6754",
                "companyId": "level1c8603",
                "userRole": "Architect/Engineer",
                "userRfiRole": "rfiAssignee"
            }
        ],
        "userPersona":
        {
            "userId": "lithiumm6754",
            "companyId": "level1c8603",
            "userRole": "Architect/Engineer",
            "userRfiRole": "rfiAssignee"
        },
        "userRfiState": "open"
    }
}

rfihistory = {
    "code": "200",
    "data": [
        {
            "id": 47,
            "createdAt": "2020-10-19T15:56:18.572377Z",
            "userId": "lithiumm6754",
            "userRfiState": "draft",
            "userRole": "foreman",
            "userRfiRole": "rfiCreator",
            "responding": False,
            "rfiId": 24
        },
        {
            "id": 48,
            "createdAt": "2020-10-19T15:56:18.578458Z",
            "userId": "carbonh3982",
            "userRfiState": "draft",
            "userRole": "projectManager",
            "userRfiRole": "rfiCreator",
            "responding": False,
            "rfiId": 24
        },
        {
            "id": 122,
            "createdAt": "2020-10-19T15:57:13.294070Z",
            "userId": "eriks9781",
            "userRfiState": "open",
            "userRole": "projectManager",
            "userRfiRole": "rfiManager",
            "responding": False,
            "rfiId": 24
        },
        {
            "id": 156,
            "createdAt": "2020-10-19T15:58:10.405750Z",
            "userId": "iodinec7573",
            "userRfiState": "open",
            "userRole": "projectManager",
            "userRfiRole": "rfiReceiver",
            "responding": False,
            "rfiId": 24
        },
        {
            "id": 183,
            "createdAt": "2020-10-19T15:58:57.540503Z",
            "userId": "arsenicd6441",
            "userRfiState": "open",
            "userRole": "Architect/Engineer",
            "userRfiRole": "rfiAssignee",
            "responding": False,
            "rfiId": 24
        },
        {
            "id": 205,
            "createdAt": "2020-10-19T15:59:32.125841Z",
            "userId": "iodinec7573",
            "userRfiState": "open",
            "userRole": "projectManager",
            "userRfiRole": "rfiReceiver",
            "responding": True,
            "rfiId": 24
        },
        {
            "id": 225,
            "createdAt": "2020-10-19T16:00:02.005153Z",
            "userId": "eriks9781",
            "userRfiState": "open",
            "userRole": "projectManager",
            "userRfiRole": "rfiManager",
            "responding": True,
            "rfiId": 24
        },
        {
            "id": 245,
            "createdAt": "2020-10-19T16:00:35.283345Z",
            "userId": "carbonh3982",
            "userRfiState": "completed",
            "userRole": "projectManager",
            "userRfiRole": "rfiCreator",
            "responding": True,
            "rfiId": 24
        }
    ]
}

project = data["data"]["projectId"].split("-")
projectId = "RFI No: "+project[1]
print(projectId)

# pdf_name = project[1]+".pdf"
pdf_name = "RFI_Report-"+project[1]+".pdf"
print(str(pdf_name))
doc = SimpleDocTemplate(pdf_name, pagesize=A4, rightMargin=0*mm,
                        leftMargin=0*mm,
                        topMargin=inch/4,
                        bottomMargin=inch/2,
                        # pageCompression=1,
                        )

elements = []

styles = getSampleStyleSheet()

drawing_static = "http://static.27.52.47.78.clients.your-server.de:8084/api/v1/files/"

datetime_object = str(datetime.now())
datetime_object = datetime_object.split(" ")
print("272", datetime_object[0], datetime_object[1])

date = datetime_object[0].split("-")
date1 = date[2]+"-"+date[1]+"-"+date[0]
print(date1)
print(len(date1))

time = datetime.now()
time1 = time.strftime("%I:%M %p")
print(len(time1))
print("283", time1)


class NumberedCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        # Change the position of this to wherever you want the page number to be
        # style_footer = ParagraphStyle(
        #     name="Normal",
        #     fontSize=10,
        #     fontName="lato_black",
        #     spaceBefore=10,
        # )

        # text_date = Paragraph("PDF Generated On: 27-10-2020 | 9.40 A.M", style=style_footer)
        # text_page_no = Paragraph("Page: {}/{}".format(self._pageNumber, page_count), style=style_footer)
        # text_power = Paragraph("Powered By", style=style_footer)
        pdfgenerated = "Report Generated On: "+date1+" | "+time1
        self.setFont("lato_black", 10)
        self.drawRightString(80 * mm, 7*mm + (0.2 * inch),
                             pdfgenerated)
        self.drawRightString(120 * mm, 7 * mm + (0.2 * inch),
                             "Page: %d/%d" % (self._pageNumber, page_count))
        self.drawRightString(172 * mm, 7 * mm + (0.2 * inch),
                             "Powered By")
        Linarc_logo = 'Linarc-Logo with text-blue-1-01.png'
        # self.drawImage(Linarc_logo, 440, 35, width=130, height=35, mask='auto')
        self.drawImage(Linarc_logo, 495, 31, width=80, height=25,
                       mask=None, preserveAspectRatio=True)
        # self.drawRightString(180 * mm, 10 * mm + (0.2 * inch), "Powered By <img src='Linarc-Logo with text-blue-1-01.png' width='100' height='35'/>")


class RotatedImage(Image):

    def wrap(self, availWidth, availHeight):
        height, width = Image.wrap(self, availHeight, availWidth)
        return width, height

    def draw(self):
        self.canv.rotate(90)
        Image.draw(self)


def compressMe(file, verbose=False):
    filepath = os.path.join(os.getcwd(),
                            file)
    # print(filepath)
    im = I1.open(filepath)
    picture = im.convert('RGB')
    file1 = file.split(".")
    # print(file1[0])
    file2 = file1[0]+".jpeg"
    picture.save("Compressed_"+file2,
                 "JPEG",
                 optimize=True,
                 quality=10)
    return


def main(file_name):
    verbose = False
    if (len(sys.argv) > 1):

        if (sys.argv[1].lower() == "-v"):
            verbose = True
    cwd = os.getcwd()
    formats = ('.jpg')
    for file in os.listdir(cwd):
        if os.path.splitext(file)[1].lower() in formats:
            if(file_name == file):
                # print('compressing', file)
                compressMe(file, verbose)
    # print("Done")


style1 = ParagraphStyle(
    name="Normal",
    fontSize=15,
    alignment=TA_CENTER,
    leading=14,
    fontName="lato_bold",
    textColor=colors.HexColor("#316BAE"),
)

style1a = ParagraphStyle(
    name="Normal",
    fontSize=15,
    alignment=TA_LEFT,
    fontName="lato_bold",
    leading=14,
    textColor=colors.HexColor("#316BAE"),
)

ts = [('LEFTPADDING', (0, 0), (-1, -1), 15)]

text1_a = Paragraph('''<b>Project : OaK Dale</b>''', style=style1a)

text1_b = Paragraph(projectId, style=style1)

colwidth = (215, 85, 270)
# text1_b = Paragraph('''<b>RFI Report  #CON1023</b>''', style=style1)
data1 = [[text1_a, "", text1_b]]
table1 = Table(data1, colwidth, style=ts, spaceAfter=10)
# table1 = Table(data1, spaceAfter=10)
table1.setStyle(TableStyle([('BOX', (-1, -1), (-1, -1), 1, colors.black),
                            ('BOTTOMPADDING', (0, 0), (-1, -1), 10),
                            ('TOPPADDING', (-1, -1), (-1, -1), 10),
                            ('LEFTPADDING', (-1, -1), (-1, -1), 5), ]))

# elements.append(Indenter(left=0.7935*cm, right=0.7935*cm))

elements.append(table1)

style2a = ParagraphStyle(
    name='Normal',
    fontSize=10,
    alignment=TA_LEFT,
    fontName="lato_bold",
    leading=10,
    textColor=colors.HexColor("#316BAE"),
)

style2 = ParagraphStyle(
    name="Normal",
    fontName="lato_black",
    fontSize=10,
    alignment=TA_LEFT,
    leading=10,
)
text2_a = Paragraph('''<b>Request By</b>''', style=style2a)
text2_b = Paragraph("Allen Construction", style=style2)

# data2 = [[text2_a, text2_b, "", ""]]

data2_a = [[text2_a, text2_b]]
table2_a = Table(data2_a)

data2 = [[table2_a, ""]]

table2 = Table(data2, style=ts)

# elements.append(Indenter(left=0.7935*cm, right=0.7935*cm))

elements.append(table2)

category1 = data["data"]["category"]
category = category1.capitalize()
subtype1 = data["data"]["subCategory"]
subtype = subtype1.capitalize()
priority = data["data"]["priority"]
duedate = data["data"]["resolvedByDate"]
cost = data["data"]["costImpact"]
schedule = data["data"]["scheduleImpact"]
date = data["data"]["createdAt"].split("T")
date_no = date[0]
print(date_no)
text3_a = Paragraph('''Type''', style=style2a)
text3_b = Paragraph('''Subtype''', style=style2a)
text3_c = Paragraph('''Priority''', style=style2a)
text3_d = Paragraph('''Open date''', style=style2a)
text3_d = Paragraph('''Due date''', style=style2a)
text3_e = Paragraph('''Location''', style=style2a)
text3_f = Paragraph('''Reference''', style=style2a)
text3_g = Paragraph('''Cost Impact''', style=style2a)
text3_h = Paragraph('''Schdule Impact''', style=style2a)
text3_i = Paragraph(category, style=style2)
text3_j = Paragraph(subtype, style=style2)
text3_k = Paragraph(priority, style=style2)
text3_l = Paragraph(duedate, style=style2)
# text3_m = Paragraph(cost, style=style2)
# text3_n = Paragraph(schedule, style=style2)
text3_o = Paragraph(date_no, style=style2)
floor = "First Floor"
cement = "Concrete"
empty_text_1 = " "
empty_text_2 = ""
text3_p = Paragraph("First Floor", style=style2)
text3_q = Paragraph(cement, style=style2)
text3_r = Paragraph("-", style=style2)
text3_s = Paragraph("-", style=style2)
text3_t = Paragraph(empty_text_1, style=style2)
text3_u = Paragraph(empty_text_1, style=style2)

data3_a = [[text3_a], [text3_b], [text3_c], [text3_d], [text3_e]]
table3_a = Table(data3_a,  style=ts)

# table3_a.setStyle(TableStyle([('BOX', (0,0), (-1,-1),1,colors.black)]))

data3_b = [[text3_i], [text3_j], [text3_k], [text3_o], [text3_l]]
table3_b = Table(data3_b)

# table3_b.setStyle(TableStyle([('BOX', (0,0), (-1,-1),1,colors.black)]))

data3_c = [[text3_e], [text3_f], [text3_g], [text3_h], [text3_u]]
table3_c = Table(data3_c)

table3_c.setStyle(TableStyle([
    # ('BOX', (0,0), (-1,-1),1,colors.black),
# ('GRID', (0,0), (-1, -1), 1, colors.black),
('TOPPADDING', (-1,-1), (-1,-1), 13),
]))

data3_d = [[text3_p], [text3_q], [text3_r], [text3_s], [text3_t]]
table3_d = Table(data3_d)

table3_d.setStyle(TableStyle([
    # ('BOX', (0,0), (-1,-1),1,colors.black),
    ('TOPPADDING', (-1,-1), (-1,-1), 13),
    ]))

data3 = [[table3_a, table3_b, table3_c, table3_d]]
table3 = Table(data3)

table3.setStyle(TableStyle([
    # ('BOX', (0, 0), (-1, -1), 1, colors.black),
    ('LINEABOVE', (0, 0), (-1, -1), 1, colors.black),
    ('LINEBELOW', (0, -1), (-1, -1), 1, colors.black),
    ('LINEBEFORE', (2, 0), (2, -1), 1, colors.black),
    ('BOTTOMPADDING', (0, 0), (-1, -1), 10)
]))

# elements.append(Indenter(left=0.7935*cm, right=0.7935*cm))

elements.append(table3)

# elements.append(Indenter(left=0.7935*cm, right=0.7935*cm))

text4_a = Paragraph('''<b>Response by</b>''', style=style2a)
text4_b = Paragraph('''<b>Response date</b>''', style=style2a)

data4_a = [[text4_a], [text4_b]]
table4_a = Table(data4_a, style=ts)

text4_c = Paragraph("Bob's Builder", style=style2)
text4_d = Paragraph("24-10-2020", style=style2)

data4_b = [[text4_c], [text4_d]]
table4_b = Table(data4_b)

data4 = [[table4_a, table4_b, "", ""]]
table4 = Table(data4)

table4.setStyle(TableStyle([('BOTTOMPADDING', (0, 0), (-1, -1), 10),
                            ('LINEBELOW', (0, -1), (-1, -1), 1, colors.black),
                            # ('BOX', (0, 0), (-1, -1), 1, colors.black)
                            ]))
elements.append(table4)

style5 = ParagraphStyle(
    name="Normal",
    fontSize=12,
    fontName="lato_black",
    # firstLineIndent=10,
    alignment=TA_JUSTIFY,
    leading=14,
    # fontName = Helvetica,
)

style6 = ParagraphStyle(
    name="Normal",
    # firstLineIndent=10,
    alignment=TA_JUSTIFY,
    fontSize=12,
    fontName="lato_black",
    leading=14,
    # fontName = Helvetica,
)

style5a = ParagraphStyle(
    name="Normal",
    spaceAfter=10,
    # spaceBefore=5,
    fontSize=12,
    fontName="lato_bold",
    leading=14,
    textColor=colors.HexColor("#316BAE"),
)

styledia1 = ParagraphStyle(
    name="Normal",
    topPadding=0,
    alignment=TA_LEFT,
    fontName="lato_black",
    textColor=colors.HexColor("#0000FF"),
)

# ts1 = [('LEFTMARGIN', (0,0), (-1, -1), 15)]

# elements.append(Indenter(left=0.7935*cm, right=0.7935*cm))
elements.append(Indenter(left=10, right=10))

questions = data["data"]["questions"]
count = 1
for a in questions:
    # print(a)
    if(count != 1):
        elements.append(PageBreak())
    texta = Paragraph('''<b>Question{}:</b>'''.format(count), style=style5a)
    question_date = a["question"]["createdAt"].split("T")
    questiondate = question_date[0]
    textb = Paragraph(questiondate, style=styles["Normal"])
    question_msg = a["question"]["questionMsg"]
    questionmsg = question_msg.split("<br>")
    question = ","
    question = question.join(questionmsg)
    textc = Paragraph(question, style=style5)
    attached_que = a["question"]["attachments"]
    print("755", len(a))

    data_a = [[texta], [textc]]
    # colWidths = (565)
    # rowHeights = (30, 480)
    table_a = Table(data_a)

    # table_a.setStyle(TableStyle([('BOX', (0,0), (-1,-1),1,colors.black)]))
    attached_ans = a["response"]["attachments"]
    # print(len(attached), "539")

    textd = Paragraph('''<b>Answer{}:</b>'''.format(count), style=style5a)
    answer_date = a["response"]["createdAt"].split("T")
    answerdate = answer_date[0]
    texte = Paragraph(answerdate, style=styles["Normal"])
    answer_reply = a["response"]["responseMsg"]
    answerreply = answer_reply.split("<br>")
    answer = ","
    answer = answer.join(answerreply)
    textf = Paragraph(answer, style=style6)

    data_b = [[textd], [textf]]
    # colWidths = (565)
    # rowHeights = (30, 480)
    table_b = Table(data_b, spaceAfter=5)
    # table_b.setStyle(TableStyle([('BOX', (0,0), (-1,-1),1,colors.black)]))

    data_c = [[table_a], [table_b]]
    table_c = Table(data_c, spaceBefore=10)

    table_c.setStyle(TableStyle([
        # ('BOX', (0, 0), (-1, -1), 2, colors.black),
        ('RIGHTPADDING', (0, 0), (-1, -1), 10),
        # ('GRID', (0,0), (-1,-1), 1, colors.black),
        ('GRID', (0, 2), (-1, -1), 1, colors.black)
        # ('LINEBELOW', (0, -1), (-1, -1), 1, colors.black)
    ]))

    elements.append(table_c)
    # elements.append(PageBreak())

    que = 1
    res = 1

    if(len(attached_que) > 0):
        elements.append(PageBreak())
        for dia in attached_que:
            if(dia["projectPlans"] == True):
                # print("questiontrue", dia["url"])
                # elements.append(PageBreak())
                # elements.append(Indenter(left=3, right=3))
                questiontrue = dia["id"]
                project_dia = drawing_static+dia["id"]+"?getThumb=0"
                styledia = ParagraphStyle(
                    name="Normal",
                    topPadding=0,
                    alignment=TA_CENTER,
                    fontName="lato_black",
                )
                styledia2 = ParagraphStyle(
                    name="Normal",
                    alignment=TA_LEFT,
                    fontName="lato_black",
                    textColor=colors.HexColor("#0000FF"),
                )
                r = requests.get(project_dia)
                name = dia["id"]+".jpg"
                # print(name)
                with open(name, "wb") as f:
                    f.write(r.content)
                
                # textdia = urllib2.urlopen(questiontrue).read()
                # logo = Image(BytesIO(textdia))
                # print(logo)
                # img = RotatedImage(textdia, width=50, height=50)
                # img.hAlign = 'CENTER'
                # textdia = Paragraph(
                #     '''<img src={} width="530" height="350" />'''.format(name), style=styledia)
                
                # print("Originalimagesize", (os.stat(name).st_size)/(1024*1024))
                filesize = (os.stat(name).st_size)/(1024*1024)
                if(filesize > 1.0):
                    main(name)
                    # print("out of function")
                    img_1 = "Compressed_"+dia["id"]+".jpeg"
                else:
                    img_1 = name
                # print("imagesize",os.path.getsize(img_1))

                # normal way of inserting the image inside the PDF.......

                # image = Image(img_1)
                # image._restrictSize(7*inch, 10*inch)

                # End of normal way of inserting image into PDF......

                image = RotatedImage(img_1,
                                     width=650, height=550)
                # image.hAlign='CENTER'
                
                click = "(Click Here For attachments)"
                address = '<link href="{}">'.format(project_dia)+click+'</link>'
                textadd = Paragraph(address, style=styledia2)
                textinp = Paragraph("Drawing Details", style=styledia)

                # data_dia = [[img], [textinp]]
                # data_dia = [[textadd], [image], [textinp]]
                data_dia = [[image], [textinp]]
                # table_dia = Table(data_dia, colWidths=(
                #     550), spaceBefore=20)

                # rowHeights1 = (500, 50)
                colWidths1 = (560)
                table_dia = Table(data_dia, colWidths1,
                                  spaceBefore=1)
                table_dia.setStyle(TableStyle([('TOPPADDING', (0, 0), (0, 0), 5),
                                               ('LEFTPADDING', (0, 0), (0, 0), 1100),
                                               ('ALIGN', (0, 0),
                                                (-1, -1), 'CENTER'),
                                               # ('RIGHTPADDING', (0,0), (0,0), 100),
                                               ('BOTTOMPADDING',
                                                (-1, -1), (-1, -1), 5),
                                               #    ('BOX', (0, 0), (-1, -1),
                                               #     1, colors.black),
                                               #    ('GRID', (0, 0), (-1, -1), 1, colors.black)
                                               ]))
                data_add = [[textadd], [table_dia]]
                table_add = Table(data_add)
                table_add.setStyle(TableStyle([('TOPPADDING', (0,0), (0,0), 5)]))

                elements.append(table_add)
                if(que < len(attached_que)):
                    plans = attached_que[que]["projectPlans"]
                    print("865", plans)
                    if(plans == True):
                        elements.append(PageBreak())
                # elements.append(PageBreak())

            else:
                # print('questionfalse', dia["url"])
                # questionfalse = dia["url"]
                # elements.append(PageBreak())
                elements.append(Indenter(left=3, right=3))
                project_dia = drawing_static+dia["id"]+"?getThumb=0"
                r = requests.get(project_dia)
                name = dia["id"]+".jpg"
                # print(name)
                with open(name, "wb") as f:
                    f.write(r.content)
                styledia = ParagraphStyle(
                    name="Normal",
                    topPadding=50,
                    alignment=TA_CENTER,
                    fontName="lato_black",
                )

                # print("Originalimagesize", (os.stat(name).st_size)/(1024*1024))
                filesize = (os.stat(name).st_size)/(1024*1024)
                # Load the Image from the URL......
                # textdia = urllib2.urlopen(questionfalse).read()
                # logo = Image(BytesIO(textdia))
                # end of loading the image from the URL.....

                # textdia = Paragraph(
                #     '''<img src={} width="530" height="350" />'''.format(name), style=styledia)
                if(filesize > 1.0):
                    main(name)
                    # print("out of function")
                    img_1 = "Compressed_"+dia["id"]+".jpeg"
                else:
                    img_1 = name
                # print("imagesize",os.path.getsize(img_1))
                image = Image(img_1)
                image._restrictSize(6*inch, 10*inch)

                click = "Click Here For attachments"
                address = '<link href="{}">'.format(project_dia)+click+'</link>'
                textadd = Paragraph(address, style=styledia1)

                textinp = Paragraph("Drawing Details", style=styledia)
                # data_dia = [ [textadd], [image],[textinp]]
                data_dia = [[image], [textinp]]

                # table_dia = Table(data_dia, colWidths=(
                # 550), spaceBefore=20)

                # rowHeights1 = (500, 50)
                colWidths1 = (560)

                table_dia = Table(data_dia, colWidths1,
                                  spaceBefore=1)

                table_dia.setStyle(TableStyle([('TOPPADDING', (0, 0), (0, 0), 5),
                                               ('ALIGN', (0, 0),
                                                (-1, -1), 'CENTER'),
                                               # ('RIGHTPADDING', (0,0), (0,0), 100),
                                               ('BOTTOMPADDING',
                                                (-1, -1), (-1, -1), 5),
                                               #    ('BOX', (0, 0), (-1, -1),
                                               #     1, colors.black),
                                               #    ('GRID', (0, 0), (-1, -1), 1, colors.black)
                                               ]))

                data_add = [[textadd], [table_dia]]
                table_add = Table(data_add)

                elements.append(table_add)
                # elements.append(PageBreak())
                if(que < len(attached_que)):
                    plans = attached_que[que]["projectPlans"]
                    print("940", plans)
                    if(plans == True):
                        elements.append(PageBreak())

            que=que+1

    # else:
    #     elements.append(PageBreak())

    if(len(attached_ans) > 0):
    
        elements.append(PageBreak())
        for dia_ans in attached_ans:
            if(dia_ans["projectPlans"] == True):
                project_dia_1 = drawing_static+dia_ans["id"]+"?getThumb=0"
                styledia = ParagraphStyle(
                    name="Normal",
                    topPadding=50,
                    alignment=TA_CENTER,
                    fontName="lato_black",
                )
                r = requests.get(project_dia_1)
                name_1 = dia_ans["id"]+".jpg"
                # print(name_1)
                with open(name_1, "wb") as f:
                    f.write(r.content)
                # textdia = urllib2.urlopen(questiontrue).read()
                # logo = Image(BytesIO(textdia))
                # print(logo)
                # img = RotatedImage(textdia, width=50, height=50)
                # img.hAlign = 'CENTER'
                # textdia_1 = Paragraph(
                #     '''<img src={} width="530" height="450" />'''.format(name_1), style=styledia)
                # print("Originalimagesize", (os.stat(name_1).st_size)/(1024*1024))
                filesize = (os.stat(name_1).st_size)/(1024*1024)
                if(filesize > 1.0):
                    main(name_1)
                    # print("out of function")
                    img_1 = "Compressed_"+dia_ans["id"]+".jpeg"
                else:
                    img_1 = name_1
                # print("imagesize",os.path.getsize(img_1))

                # Normal way of inserting the image inside the PDF.....
                # image=Image(img_1)
                # image._restrictSize(7*inch, 10*inch)

                # End of Normal way of inserting image inside the PDF....

                image = RotatedImage(img_1, width=650, height=550)
                # image.hAlign='CENTER'

                click = "Click Here For attachments"
                address = '<link href="{}">'.format(project_dia_1)+click+'</link>'
                textadd = Paragraph(address, style=styledia1)

                textinp_1 = Paragraph("Drawing Details", style=styledia)
                # data_dia = [[img], [textinp]]
                # data_dia_1 = [[textadd], [image],[textinp_1]]
                data_dia_1 = [[image], [textinp_1]]
                # table_dia = Table(data_dia, colWidths=(
                #     550), spaceBefore=20)

                # rowHeights1 = (650, 50)
                colWidths1 = (560)
                table_dia_1 = Table(data_dia_1, colWidths1,
                                    spaceBefore=1)
                table_dia_1.setStyle(TableStyle([('TOPPADDING', (0, 0), (0, 0), 5),
                                                 ('LEFTPADDING',
                                                  (0, 0), (0, 0), 1100),
                                                 ('ALIGN', (0, 0),
                                                  (-1, -1), 'CENTER'),
                                                 # ('RIGHTPADDING', (0,0), (0,0), 100),
                                                 ('BOTTOMPADDING',
                                                  (-1, -1), (-1, -1), 5),
                                                 #  ('BOX', (0, 0), (-1, -1),
                                                 #   1, colors.black),
                                                 #  ('GRID', (0, 0), (-1, -1), 1, colors.black)
                                                 ]))

                data_add = [[textadd], [table_dia_1]]
                table_add = Table(data_add)
                table_add.setStyle(TableStyle([('TOPPADDING', (0,0), (0,0), 5)]))
                elements.append(table_add)
                if(res < len(attached_ans)):
                    plans = attached_ans[res]["projectPlans"]
                    print("1024", plans)
                    if(plans == True):
                        elements.append(PageBreak())
                # elements.append(PageBreak())
            else:
                # print("answerfalse", dia_ans["id"])
                elements.append(Indenter(left=3, right=3))
                project_dia_1 = drawing_static+dia_ans["id"]+"?getThumb=0"
                r = requests.get(project_dia_1)
                name_1 = dia_ans["id"]+".jpg"
                # print(name_1)
                with open(name_1, "wb") as f:
                    f.write(r.content)
                styledia = ParagraphStyle(
                    name="Normal",
                    topPadding=50,
                    alignment=TA_CENTER,
                    fontName="lato_black",
                )
                # print("Originalimagesize", (os.stat(name_1).st_size)/(1024*1024))
                filesize = (os.stat(name_1).st_size)/(1024*1024)

                # Load the Image from the URL......
                # textdia = urllib2.urlopen(questionfalse).read()
                # logo = Image(BytesIO(textdia))
                # end of loading the image from the URL.....

                # textdia_1 = Paragraph(
                #     '''<img src={} width="530" height="450" />'''.format(name_1), style=styledia)
                if(filesize > 1.0):
                    main(name_1)
                    # print("out of function")
                    img_1 = "Compressed_"+dia_ans["id"]+".jpeg"
                else:
                    img_1 = name_1
                # print("imagesize", os.path.getsize(img_1))

                image = Image(img_1)
                image._restrictSize(6*inch, 10*inch)

                click = "Click Here For attachments"
                address = '<link href="{}">'.format(project_dia_1)+click+'</link>'
                textadd = Paragraph(address, style=styledia1)

                textinp_1 = Paragraph("Drawing Details", style=styledia)
                # data_dia_1 = [ [textadd], [image],[textinp_1]]
                data_dia_1 = [[image], [textinp_1]]

                # table_dia = Table(data_dia, colWidths=(
                # 550), spaceBefore=20)

                # rowHeights1 = (650, 50)
                colWidths1 = (560)

                table_dia_1 = Table(data_dia_1, colWidths1,
                                    spaceBefore=1)

                table_dia_1.setStyle(TableStyle([('TOPPADDING', (0, 0), (0, 0), 5),
                                                 ('ALIGN', (0, 0),
                                                  (-1, -1), 'CENTER'),
                                                 # ('RIGHTPADDING', (0,0), (0,0), 100),
                                                 ('BOTTOMPADDING',
                                                  (-1, -1), (-1, -1), 5),
                                                 #  ('BOX', (0, 0), (-1, -1),
                                                 #   1, colors.black),
                                                 #  ('GRID', (0, 0), (-1, -1), 1, colors.black)
                                                 ]))

                data_add = [[textadd], [table_dia_1]]
                table_add = Table(data_add)
                elements.append(table_add)
                if(res < len(attached_ans)):
                    plans = attached_ans[res]["projectPlans"]
                    print("1095", plans)
                    if(plans == True):
                        elements.append(PageBreak())
                # elements.append(PageBreak())
            res = res+1

    # else:
    #     elements.append(PageBreak())

    count = count+1

style4a = ParagraphStyle(
    name='Normal',
    fontSize=10,
    alignment=TA_LEFT,
    fontName="lato_bold",
    leading=14,
)

elements.append(PageBreak())

elements.append(Indenter(left=3))


stylehistory = ParagraphStyle(
    name='Normal',
    fontSize=10,
    alignment=TA_LEFT,
    fontName="lato_bold",
    leading=14,
    textColor=colors.HexColor("#316BAE"),
)

text_rfi = Paragraph("RFI History", style=style2a)

datarfi = [[text_rfi, ""]]
table_rfi = Table(datarfi)

elements.append(table_rfi)

elements.append(Indenter(left=3))

text5_a = Paragraph('''USER_ID''', style=style4a)
text5_b = Paragraph('''RECEIVED-AT''', style=style4a)
text5_c = Paragraph('''RESOLVED-AT''', style=style4a)

data13 = [[text5_a, text5_b, text5_c]]

history = rfihistory["data"]
i = 0
for a in history:
    created_at = a["createdAt"].split("T")
    created_time = created_at[1].split(".")
    time2 = created_time[0].split(":")
    time_2 = time2[0]+":"+time2[1]
    d3 = datetime.strptime(time_2, "%H:%M")
    d4 = d3.strftime("%I:%M %p")
    resolveddate = created_at[0]+" | "+d4
    text13_a = Paragraph(resolveddate, style=style2)
    user_id = a["userId"]
    text13_b = Paragraph(user_id, style=style2)
    if(i != len(history)-1):
        # j =i+1
        a1 = rfihistory["data"][i+1]["createdAt"].split("Z")
        # print(a1)
        resolved_at = rfihistory["data"][i+1]["createdAt"].split("T")
        resolved_time = resolved_at[1].split(".")
        resolved_time_1 = resolved_time[0].split(":")
        resolvedtime_1 = resolved_time_1[0]+":"+resolved_time_1[1]
        d = datetime.strptime(resolvedtime_1, "%H:%M")
        d1 = d.strftime("%I:%M %p")
        resolvedat = resolved_at[0]+" | "+d1
        text13_f = Paragraph(resolvedat, style=style2)
        data13_a = [text13_b, text13_a, text13_f]
    else:
        resolved_at = "-"
        text13_f = Paragraph(resolved_at, style=style2)
        data13_a = [text13_b, text13_a, text13_f]

    data13.append(data13_a)

    i = i+1

colwidths2 = (280, 130, 130)

table13 = Table(data13, colwidths2,  style=ts, spaceBefore=10)


table13.setStyle(TableStyle([('BOX', (0, 0), (-1, -1), 1, colors.black),
                             ('GRID', (0, 0), (-1, -1), 1, colors.black),
                             ('BOTTOMPADDING', (0, 0), (-1, -1), 20),
                             ('TOPPADDING', (0, 0), (-1, -1), 15)]))

elements.append(table13)

gc.collect()

doc.build(elements, canvasmaker=NumberedCanvas)
